//
//  NetworkProtocol.swift
//  FutureWorkshopsApp
//
//  Created by Cristian Barril on 07/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

typealias jsonObject = [String:Any]
typealias FetchDataCompletion = (_ responseObject:jsonObject) -> ()
typealias FetchDataCompletionError = (_ error: ServerError) -> ()

enum ServerError: Int {
    case Unauthorized = 401
    case NotFound = 404
    case Internal = 500
    case unknown = -999
}

protocol NetworkClient {
    func get(baseURL: String, uri: String, params:[String: Any]?, completitionHandler:@escaping FetchDataCompletion, errorHandler:@escaping FetchDataCompletionError)
}
