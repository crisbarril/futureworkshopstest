//
//  Date.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 03/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

extension Date {
    func dateFromString(_ dateString: String) -> Date {
        let dateFormatter = getDateFormatter()
        
        if let formattedDate = dateFormatter.date(from: dateString) {
            return formattedDate
        }
        else {
            fatalError("ERROR: Date conversion failed due to mismatched format.")
        }
    }
    
    func stringFromDate() -> String {
        let dateFormatter = getDateFormatter()
        
        return dateFormatter.string(from: self)
    }
    
    private func getDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00")
        
        return dateFormatter
    }
}
