//
//  UIViewController.swift
//  FutureWorkshopsApp
//
//  Created by Cristian Barril on 07/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func hideNavigationBar(){
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    func showNavigationBar() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func showLoadingActivityIndicatorView() -> UIActivityIndicatorView {
        let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        activityIndicator.color = UIColor.primaryColor()
        activityIndicator.center = self.view.center
        activityIndicator.startAnimating()
        
        self.view.addSubview(activityIndicator)
        
        return activityIndicator
    }
    
    func hideLoading(activityIndicatorView : UIActivityIndicatorView) {
        DispatchQueue.main.async {
            activityIndicatorView.removeFromSuperview()
        }
    }
    
    func showAlert(_ title: String, message msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        let action = UIAlertAction(title: "Close", style: .default, handler: nil)
        alert.addAction(action)
        
        self.present(alert, animated: true, completion: nil)
    }
}
