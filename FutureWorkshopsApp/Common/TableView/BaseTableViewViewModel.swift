//
//  BaseTableViewViewModel.swift
//  FutureWorkshopsApp
//
//  Created by Cristian Barril on 07/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import UIKit

@objc protocol BaseTableViewViewModel {
    
    var title: String { get }
    var sections: Int { get }
    
    func rows(_ forSection: Int) -> Int
    func objectForRow(_ indexPath: IndexPath) -> Any
    func cellHeightForRow(_ indexPath: IndexPath) -> CGFloat
    @objc optional func objectForSection(_ indexPath: IndexPath) -> Any
    @objc optional func titleForHeaderInSection(_ section: Int) -> String
}
