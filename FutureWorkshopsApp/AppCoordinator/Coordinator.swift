//
//  Coordinator.swift
//  FutureWorkshopsApp
//
//  Created by Cristian Barril on 07/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import UIKit

protocol Coordinator: class {
    func start(navigationController: UINavigationController?)
}

protocol SessionDelegate: class {
    func login(_ username: String)
    func logout()
}

protocol NewsMainViewDelegate: class {
    func didSelect(_ article: ArticleList, viewModel: NewsMainViewModel)
}

protocol DetailNewsViewDelegate: class {
    func willDismiss(_ article: ArticleDetail)
}
