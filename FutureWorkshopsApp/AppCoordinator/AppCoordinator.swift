//
//  AppCoordinator.swift
//  FutureWorkshopsApp
//
//  Created by Cristian Barril on 07/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import UIKit

class AppCoordinator: Coordinator {
    
    internal var navigationController: UINavigationController?
    internal var currentState: StateMachine = SessionStateMachineFactory.initialState() {
        willSet {
            currentState.leaveState()
        }
        didSet {
            currentState.enterState()
        }
    }
    
    init() {
        DatabaseManager.initDatabase()
    }
    
    func start(navigationController: UINavigationController?) {
        self.navigationController = navigationController
        
        if currentState is LoggedStateMachine {
            if let currentUser = DatabaseManager.getCurrentUser() {
                presentNewsMainView(currentUser)
            }
            else {
                print("Fail to recover user from database. Showing login view.")
                logout()
                presentLoginView()
            }
        }
        else {
            presentLoginView()
        }
    }
    
    private func presentLoginView() {
        let viewModel = LoginViewModel()
        let viewController = LoginViewController(viewModel: viewModel, loginViewDelegate: self)
        navigationController?.pushViewController(viewController, animated: true)
    }
}

extension AppCoordinator: SessionDelegate {
    
    func login(_ username: String) {
        if let currentUser = DatabaseManager.createNewUser(named: username) {
            currentState = LoggedStateMachine()
            presentNewsMainView(currentUser)
        }
        else {
            navigationController?.topViewController?.showAlert("Error", message: "Fail to create new user")
        }
    }
    
    func logout() {
        currentState = UnloggedStateMachine()
        DatabaseManager.deleteAllObjects()
        navigationController?.viewControllers.removeAll()
        presentLoginView()
    }
    
    private func presentNewsMainView(_ user: User) {
        let articlesListViewModel = NewsMainViewModel(user)
        let viewController = NewsMainViewController(viewModel: articlesListViewModel, viewDelegate: self, sessionDelegate: self)
        
        navigationController?.pushViewController(viewController, animated: true)
        
        let activityIndicator = viewController.showLoadingActivityIndicatorView()
        
        NewsMainDatasource.fetchData(completionHandler: { (articlesList) in
            articlesListViewModel.articles = articlesList
            viewController.setViewModel(articlesListViewModel)
            viewController.hideLoading(activityIndicatorView: activityIndicator)
        }) { (serverError) in
            viewController.hideLoading(activityIndicatorView: activityIndicator)
            viewController.showAlert("Error", message: "Request failed with error: \(serverError)")
        }
    }
}

extension AppCoordinator: NewsMainViewDelegate {
    
    func didSelect(_ article: ArticleList, viewModel: NewsMainViewModel) {
        
        viewModel.isTableEnabled = false
        
        DetailNewsDatasource.fetchData(id: "\(article.id)", completionHandler: { (articleDetail) in
            let articleDetailViewModel = DetailNewsViewModel(articleDetail)
            if let currentUser = DatabaseManager.getCurrentUser() {
                articleDetailViewModel.setArticle(isFavorite: currentUser.isArticleFavorite(articleId: "\(article.id)") != nil)
            }

            DispatchQueue.main.async {
                let viewController = DetailNewsViewController(viewModel: articleDetailViewModel, viewDelegate: self)
                self.navigationController?.pushViewController(viewController, animated: true)
            }
            
            viewModel.isTableEnabled = true
        }) { (serverError) in
            DispatchQueue.main.async {
                self.navigationController?.visibleViewController?.showAlert("Error", message: "Request failed with error: \(serverError)")
            }
            
            viewModel.isTableEnabled = true
        }
    }
}

extension AppCoordinator: DetailNewsViewDelegate {
    func willDismiss(_ article: ArticleDetail) {
        DatabaseManager.updateArticleStatus(article: article)
    }
}
