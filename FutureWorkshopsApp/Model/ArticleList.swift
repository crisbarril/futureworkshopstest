//
//  ArticleList.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 01/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

public class ArticleList {
    public var id: Int = 0
    public var title: String = ""
    public var icon_url: String = ""
    public var summary: String = ""
    public var content: String = ""
    public var date: Date? = nil
    
    convenience init?(json: jsonObject) {
        self.init()
        guard let id = json["id"] as? Int else {
            print("Error: Fail to recover id value")
            return nil
        }
        
        guard let title = json["title"] as? String else {
            print("Error: Fail to recover title value")
            return nil
        }
        
        guard let icon_url = json["icon_url"] as? String else {
            print("Error: Fail to recover icon_url value")
            return nil
        }
        
        guard let summary = json["summary"] as? String else {
            print("Error: Fail to recover summary value")
            return nil
        }
        
        guard let content = json["content"] as? String else {
            print("Error: Fail to recover content value")
            return nil
        }
        
        guard let date = json["date"] as? String else {
            print("Error: Fail to recover date value")
            return nil
        }
        
        self.id = id
        self.title = title
        self.icon_url = icon_url
        self.summary = summary
        self.content = content
        self.date = Date().dateFromString(date)
    }
}
