//
//  User.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 01/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation
import CoreData

extension User {
    internal func isArticleFavorite(articleId: String) -> FavoriteArticle? {
        if let articles = favoritesArticles {
            for article in articles {
                if let favoriteArticle = article as? FavoriteArticle, favoriteArticle.id == articleId {
                    return favoriteArticle
                }
            }
        }
        return nil
    }
}
