//
//  ArticleDetail.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 01/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

public class ArticleDetail {
    public var id: Int = 0
    public var title: String = ""
    public var image_url: String = ""
    public var source: String = ""
    public var content: String = ""
    public var date: Date? = nil
    public var isFavorite: Bool = false
    
    convenience init?(json: jsonObject) {
        self.init()
        guard let id = json["id"] as? Int else {
            print("Error: Fail to recover id value")
            return nil
        }
        
        guard let title = json["title"] as? String else {
            print("Error: Fail to recover title value")
            return nil
        }
        
        guard let image_url = json["image_url"] as? String else {
            print("Error: Fail to recover image_url value")
            return nil
        }
        
        guard let source = json["source"] as? String else {
            print("Error: Fail to recover source value")
            return nil
        }
        
        guard let content = json["content"] as? String else {
            print("Error: Fail to recover content value")
            return nil
        }
        
        guard let date = json["date"] as? String else {
            print("Error: Fail to recover date value")
            return nil
        }
        
        self.id = id
        self.title = title
        self.image_url = image_url
        self.source = source
        self.content = content
        self.date = Date().dateFromString(date)
    }
}
