//
//  Article.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 01/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

public protocol Article {
    var id: Int { get set }
    var title: String { get set }
    var content: String { get set }
    var date: Date? { get set }
}
