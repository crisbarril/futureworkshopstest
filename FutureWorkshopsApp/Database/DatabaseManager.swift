//
//  DatabaseManager.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 08/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import PersistenceFramework
import CoreData

class DatabaseManager {
    private static let databaseName = "FutureWorkshopsApp"
    internal static var bundle = Bundle.main
    
    internal static func initDatabase() {
        guard let modelURL = bundle.url(forResource: "DataModel", withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        
        do {
            try CoreDataManagerInit(databaseName: databaseName, bundle: bundle, modelURL: modelURL)
            NotificationCenter.default.addObserver(self, selector: #selector(saveContext), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        } catch  {
            assertionFailure("Fail to init database named \(databaseName) with error: \(error)")
        }
    }
    
    internal static func createNewUser(named name: String) -> User? {
        if let context = CoreDataManager_getContext(databaseName: databaseName, forBundle: bundle) {
            if let newUser: User = NSEntityDescription.insertNewObject(forEntityName: "User", into: context) as? User {
                newUser.name = name
                return newUser
            }
            else {
                print("Fail to create new user")
            }
        }
        else {
            print("Fail to recover context")
        }
        
        return nil
    }
    
    internal static func createNewFavoriteArticle(_ id: String) -> FavoriteArticle? {
        if let context = CoreDataManager_getContext(databaseName: databaseName, forBundle: bundle) {
            if let newFavoriteArticle: FavoriteArticle = NSEntityDescription.insertNewObject(forEntityName: "FavoriteArticle", into: context) as? FavoriteArticle {
                newFavoriteArticle.id = id
                return newFavoriteArticle
            }
            else {
                print("Fail to create new favorite article")
            }
        }
        else {
            print("Fail to recover context")
        }
        
        return nil
    }
    
    internal static func getCurrentUser() -> User? {
        if let context = CoreDataManager_getContext(databaseName: databaseName, forBundle: bundle) {
            let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            
            do {
                if let fetchedResult = try context.fetch(fetch) as? [User] {
                    if fetchedResult.count == 1 {
                        return fetchedResult[0]
                    }
                }
            } catch {
                print("Trying to fetch in context fail with error: \(error)")
                return nil
            }
        }
        else {
            print("Fail to recover context")
        }
        
        return nil
    }
    
    
    
    static internal func updateArticleStatus(article: ArticleDetail) {
        if let currentUser = getCurrentUser() {
            if article.isFavorite {
                if currentUser.isArticleFavorite(articleId: "\(article.id)") == nil {
                    if let favoriteArticle = createNewFavoriteArticle("\(article.id)") {
                        currentUser.addToFavoritesArticles(favoriteArticle)
                        print("Article \(article.id) now is favorite")
                    }
                }
            }
            else if let favoriteArticle = currentUser.isArticleFavorite(articleId: "\(article.id)") {
                currentUser.removeFromFavoritesArticles(favoriteArticle)
                deleteFavoriteArticle(favoriteArticle: favoriteArticle)
                print("Article \(article.id) now not longer favorite")
            }
        }
        else {
            print("Fail to recover current user")
        }
    }
    
    internal static func deleteAllObjects() {
        if let context = CoreDataManager_getContext(databaseName: databaseName, forBundle: bundle) {
            let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
            let request = NSBatchDeleteRequest(fetchRequest: fetch)
            do {
                try context.execute(request)
            } catch {
                print("Fail to delete entities. Delete database file instead.")
                do {
                    try CoreDataManager_deleteAllCoreData()
                    initDatabase()
                } catch {
                    fatalError("Error deleting database files with error: \(error)")
                }
            }
        }
        else {
            print("Fail to recover context")
        }
    }
    
    internal static func deleteDatabase() throws {
        do {
            try CoreDataManager_deleteAllCoreData()
        } catch  {
            throw error
        }
    }
    
    @objc internal static func saveContext() {
        do {
            try CoreDataManager_saveContext(databaseName: databaseName, forBundle: bundle)
        } catch {
            print("Fail to save context")
        }
    }
    
    // MARK: - Privates methods
    private static func deleteFavoriteArticle(favoriteArticle: FavoriteArticle) {
        if let context = CoreDataManager_getContext(databaseName: databaseName, forBundle: bundle), let id = favoriteArticle.id {
            let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: "FavoriteArticle")
            fetch.predicate = NSPredicate(format: "id = \(id)")
            
            let request = NSBatchDeleteRequest(fetchRequest: fetch)
            
            do {
                try context.execute(request)
            } catch {
                print("Fail to delete FavoriteArticle entities. Error: \(error)")
            }
        }
        else {
            print("Fail to recover context")
        }
    }
}
