//
//  LoginStateMachine.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 03/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

internal let sessionStateKey = "SessionStateKey"

struct SessionStateMachineFactory {
    
    static func initialState() -> StateMachine {
        let userDefaults = UserDefaults.standard
        let sessionActive = userDefaults.bool(forKey: sessionStateKey)
        
        if sessionActive {
            return LoggedStateMachine()
        }
        else {
            return UnloggedStateMachine()
        }
    }
}

struct LoggedStateMachine: StateMachine {
    
    func enterState() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(true, forKey: sessionStateKey)
        print("User logged in")
    }
    
    func leaveState() {
        //Actions to be taken when the user stops being logged
    }
}

struct UnloggedStateMachine: StateMachine {
    
    func enterState() {
        let userDefaults = UserDefaults.standard
        userDefaults.set(false, forKey: sessionStateKey)
        print("User logged out")
    }
    
    func leaveState() {
        //Actions to be taken when the user stops being unlogged
    }
}
