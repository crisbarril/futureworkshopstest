//
//  StateMachine.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 03/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

protocol StateMachine {

    func enterState()
    func leaveState()
}
