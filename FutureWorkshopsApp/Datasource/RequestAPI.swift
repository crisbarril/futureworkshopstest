//
//  RequestAPI.swift
//  FutureWorkshopsApp
//
//  Created by Cristian Barril on 07/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import UIKit

public typealias ArticlesCompletionHandler = (_ articlesData: [ArticleList]) -> Void
public typealias ArticleDetailCompletionHandler = (_ articleDetail: ArticleDetail) -> Void

class RequestAPI {
    
    static let sharedInstance = RequestAPI(networkClient: NetworkClientNative())
    
    private let baseURL = "https://s3.amazonaws.com/future-workshops/"
    private let urlListOfNews = "fw-coding-test.json"
    private let urlArticleDetail = "{id}.json"
    
    private var networkClient: NetworkClient
    
    init(networkClient: NetworkClient) {
        self.networkClient = networkClient
    }

    func getListOfNews(completionHandler:@escaping ArticlesCompletionHandler, errorHandler:@escaping FetchDataCompletionError) {
        
        networkClient.get(baseURL: baseURL, uri: urlListOfNews, params: nil, completitionHandler: { (response) in
            
            var articlesData = [ArticleList]()
            
            guard let articlesJsonData = response["articles"] as? [jsonObject] else {
                print("Could not get articles from response")
                errorHandler(ServerError.unknown)
                return
            }
            
            for articleJson in articlesJsonData {
                if let article = ArticleList(json: articleJson) {
                    articlesData.append(article)
                }
            }
            
            completionHandler(articlesData)
            
        }) { (err) in
            errorHandler(err)
        }
    }
    
    func getArticleDetail(_ id: String, completionHandler:@escaping ArticleDetailCompletionHandler, errorHandler:@escaping FetchDataCompletionError) {
        
        networkClient.get(baseURL: baseURL, uri: urlArticleDetail.replacingOccurrences(of: "{id}", with: id), params: nil, completitionHandler: { (response) in
            
            if let articleDetail = ArticleDetail(json: response) {
                completionHandler(articleDetail)
            }
            else {
                errorHandler(ServerError.unknown)
            }
            
        }) { (err) in
            errorHandler(err)
        }
    }
}
