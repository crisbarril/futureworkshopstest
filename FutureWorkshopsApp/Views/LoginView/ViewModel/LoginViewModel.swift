//
//  LoginViewModel.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 28/02/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

struct LoginViewModel {
    
    private var userName = ""
    
    public func loginImage() -> String {
        return "LoginImage"
    }
    
    public mutating func textDidChange(_ text: String) {
        userName = text
    }
    
    public func validateState() -> Bool {
        return userName.count > 0
    }
    
    public func getUserName() -> String {
        return userName
    }
}
