//
//  LoginViewController.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 28/02/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    @IBOutlet weak var loginImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!

    private weak var loginViewDelegate: SessionDelegate?
    private var loginViewModel: LoginViewModel
    
    init(viewModel: LoginViewModel, loginViewDelegate: SessionDelegate) {
        self.loginViewModel = viewModel
        self.loginViewDelegate = loginViewDelegate
        super.init(nibName: String(describing: LoginViewController.self), bundle: Bundle(for: type(of: self)))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        hideNavigationBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        showNavigationBar()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    private func initView() {
        //Textfield
        nameTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        nameTextField.delegate = self
        
        //Keyboards notifications
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        //Remove keyboard action
        self.hideKeyboardWhenTappedAround()
        
        //Image
        loginImageView.image = UIImage.init(named: loginViewModel.loginImage())
        
        //Button design
        loginButton.backgroundColor = UIColor.primaryColor()
        loginButton.layer.cornerRadius = 5
    }
    
    @IBAction func loginActionButton(_ sender: Any) {
        login()
    }
    
    private func login() {
        if loginViewModel.validateState() {
            self.loginViewDelegate?.login(loginViewModel.getUserName())
        }
    }
}

extension LoginViewController: UITextFieldDelegate {
    
    @objc private func textFieldDidChange(_ textField: UITextField) {
        if let textString = textField.text {
            loginViewModel.textDidChange(textString)
        }
    }
    
    @objc internal func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        login()
        return true
    }
}

extension LoginViewController {
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        let verticalPadding: CGFloat = 20
        let loginButtonInverseVerticalPosition = self.view.frame.size.height - (loginButton.frame.origin.y + loginButton.frame.size.height + verticalPadding)
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.size {
            if self.view.frame.origin.y == 0 && loginButtonInverseVerticalPosition < keyboardSize.height {
                self.view.frame.origin.y -= keyboardSize.height - loginButtonInverseVerticalPosition
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if self.view.frame.origin.y != 0 {
            self.view.frame.origin.y = 0
        }
    }
}
