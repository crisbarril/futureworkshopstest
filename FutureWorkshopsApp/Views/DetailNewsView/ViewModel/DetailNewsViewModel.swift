//
//  DetailNewsViewModel.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 11/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

struct DetailNewsViewModel {
    
    var article: ArticleDetail
    
    init(_ article: ArticleDetail) {
        self.article = article
    }
    
    internal func getArticleImage() -> String {
        return article.image_url
    }
    
    internal func getArticlePlaceholder() -> String {
        return "placeholder-image"
    }
    
    internal func getViewTitle() -> String {
        return article.title
    }
    
    internal func getArticleTitle() -> String {
        return article.title
    }
    
    internal func getArticleSource() -> String {
        return article.source
    }
    
    internal func getArticleDate() -> Date? {
        return article.date
    }
    
    internal func getArticleContent() -> String {
        return article.content
    }
    
    internal func didPressFavoriteButton() {
        article.isFavorite = !article.isFavorite
    }
    
    internal func getFavoriteButtonImage() -> String {
        return article.isFavorite ? "ic_star_white" : "ic_star_border_white"
    }
    
    internal func setArticle(isFavorite: Bool) {
        article.isFavorite = isFavorite
    }
}
