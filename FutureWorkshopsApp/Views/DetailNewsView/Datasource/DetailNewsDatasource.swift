//
//  DetailNewsDatasource.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 11/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

struct DetailNewsDatasource {
    
    static func fetchData(id: String, completionHandler:@escaping ArticleDetailCompletionHandler, errorHandler:@escaping FetchDataCompletionError) {
        RequestAPI.sharedInstance.getArticleDetail(id, completionHandler: completionHandler, errorHandler: errorHandler)
    }
}
