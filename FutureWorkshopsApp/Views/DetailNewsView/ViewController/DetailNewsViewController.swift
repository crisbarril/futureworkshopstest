//
//  DetailNewsViewController.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 11/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import UIKit

class DetailNewsViewController: UIViewController {
    
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleTitle: UILabel!
    @IBOutlet weak var articleSubtitle: UILabel!
    @IBOutlet weak var articleContent: UITextView!
    
    weak var viewDelegate: DetailNewsViewDelegate?
    private var viewModel: DetailNewsViewModel
    
    init(viewModel: DetailNewsViewModel, viewDelegate: DetailNewsViewDelegate?) {
        self.viewModel = viewModel
        self.viewDelegate = viewDelegate
        super.init(nibName: "DetailNewsViewController", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        viewDelegate?.willDismiss(viewModel.article)
    }
    
    private func initView() {
        self.title = viewModel.getViewTitle()
        articleImage.image = UIImage(named: viewModel.getArticlePlaceholder())
        articleImage.downloadedFrom(urlString: viewModel.getArticleImage())
        articleTitle.text = viewModel.getViewTitle()
        articleSubtitle.text = "\(viewModel.getArticleSource()) - \(viewModel.getArticleDate()?.stringFromDate() ?? "no date")"
        articleContent.text = viewModel.getArticleContent()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: viewModel.getFavoriteButtonImage()), style: .plain, target: self, action: #selector(favoriteAction))
    }
    
    @objc func favoriteAction() {
        viewModel.didPressFavoriteButton()
        navigationItem.rightBarButtonItem?.image = UIImage(named: viewModel.getFavoriteButtonImage())
    }
}
