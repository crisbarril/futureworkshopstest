//
//  HomeViewController.swift
//  FutureWorkshopsApp
//
//  Created by Cristian Barril on 07/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import UIKit

class NewsMainViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    weak var viewDelegate: NewsMainViewDelegate?
    weak var sessionDelegate: SessionDelegate?
    
    private var viewModel: NewsMainViewModel {
        didSet {
            DispatchQueue.main.sync {
                self.tableView.reloadData()
            }
        }
    }
    
    init(viewModel: NewsMainViewModel, viewDelegate: NewsMainViewDelegate?, sessionDelegate: SessionDelegate?) {
        self.viewModel = viewModel
        self.viewDelegate = viewDelegate
        self.sessionDelegate = sessionDelegate
        super.init(nibName: "NewsMainViewController", bundle: Bundle.main)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
    }
    
    private func initView() {
        self.title = viewModel.title
        
        let cellNib = UINib(nibName: "ArticleTableViewCell", bundle: Bundle.main)
        tableView.register(cellNib, forCellReuseIdentifier: cellIdentifier)
        tableView.dataSource = self
        tableView.delegate = self
        
        self.navigationItem.setHidesBackButton(true, animated:true)
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(logoutAction))
    }
    
    func setViewModel(_ viewModel: NewsMainViewModel) {
        self.viewModel = viewModel
    }
    
    @objc func logoutAction() {
        sessionDelegate?.logout()
    }
}

extension NewsMainViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.rows(section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! ArticleTableViewCell
        let cellData = viewModel.objectForRow(indexPath)
        cell.setup(cellData)
        
        return cell
    }
}

extension NewsMainViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.isTableEnabled, let article = viewModel.objectForRow(indexPath) as? ArticleList {
            viewDelegate?.didSelect(article, viewModel: viewModel)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return viewModel.cellHeightForRow(indexPath)
    }
}
