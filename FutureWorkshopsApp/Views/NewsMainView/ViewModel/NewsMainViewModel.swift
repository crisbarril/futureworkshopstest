//
//  HomeViewModel.swift
//  FutureWorkshopsApp
//
//  Created by Cristian Barril on 07/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import UIKit

class NewsMainViewModel {
    
    private let currentUser: User
    var articles = [ArticleList]()
    var isTableEnabled = true
    
    init(_ currentUser: User) {
        self.currentUser = currentUser
    }
}

extension NewsMainViewModel: BaseTableViewViewModel {
    
    var title: String {
        if let name = currentUser.name {
            return "\(name)'s news feed"
        }
        else {
            print("Fail to recover user name")
            return "News feed"
        }
    }
    
    var sections: Int {
        return 1
    }
    
    func rows(_ forSection: Int) -> Int {
        return articles.count
    }
    
    func cellHeightForRow(_ indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func objectForRow(_ indexPath: IndexPath) -> Any {
        return articles[indexPath.row]
    }
}
