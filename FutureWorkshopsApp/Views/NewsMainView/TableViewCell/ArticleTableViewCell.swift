//
//  MatchTableViewCell.swift
//  FutureWorkshopsApp
//
//  Created by Cristian Barril on 07/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import UIKit

let rowHeight: CGFloat = 55.0
let cellIdentifier = "cell"

class ArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        articleImageView.image = UIImage(named: "placeholder-image")
        dateLabel.text = ""
        titleLabel.text = ""
        summaryLabel.text = ""
    }
    
    public func setup(_ data: Any) {
        if let articleData = data as? ArticleList {
            articleImageView.downloadedFrom(urlString: articleData.icon_url)
            dateLabel.text = articleData.date != nil ? articleData.date!.stringFromDate() : ""
            titleLabel.text = articleData.title
            summaryLabel.text = articleData.summary
        }
    }
}
