//
//  HomeDatasource.swift
//  FutureWorkshopsApp
//
//  Created by Cristian Barril on 07/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

struct NewsMainDatasource {
    
    static func fetchData(completionHandler:@escaping ArticlesCompletionHandler, errorHandler:@escaping FetchDataCompletionError) {
        RequestAPI.sharedInstance.getListOfNews(completionHandler: completionHandler, errorHandler: errorHandler)
    }
}
