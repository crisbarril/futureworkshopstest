# FutureWorkshopsApp

Project details
---------------
* Modularize the persistence layer in it's own Cocoapods project, with a 83,85% of Unit Tests coverage
* Worked in some UX details like creating an icon with Adobe Illustrator and setting a primary color for the app
* Implement an AppCoordinator to manage the application flow. Maybe in this project it doesn’t make the difference but it’s a good pattern to abstract the presentation of the views.
* About the Api communication, I separate the business logic (RequestAPI class) and the network layer (NetworkClientNative class), using the NetworkClient protocol. This way you can easily change the network client and no affect the business logic, as you can see when I use the MockNetwork struct in Unit Test.
* For the views I use an MVVM pattern, which let you have single responsibilities classes that are more easy to understand and maintain.
* Achieve 56.32% of Unit Test coverage.