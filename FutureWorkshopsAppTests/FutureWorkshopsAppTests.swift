//
//  FutureWorkshopsAppTests.swift
//  FutureWorkshopsAppTests
//
//  Created by Cristian on 28/02/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import XCTest
@testable import FutureWorkshopsApp

class FutureWorkshopsAppTests: XCTestCase {
    
    var coordinator: AppCoordinator!
    var navigationController: UINavigationController!
    private let testUserName = "testUserName"
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        DatabaseManager.bundle = Bundle(for: FutureWorkshopsAppTests.self)
        coordinator = AppCoordinator()
        navigationController = UINavigationController()
        coordinator.currentState = UnloggedStateMachine()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        coordinator = nil
        navigationController = nil
        try? DatabaseManager.deleteDatabase()
        super.tearDown()
    }
    
    func test_01_AppCoordinator_Init() {
        coordinator.start(navigationController: navigationController)
        XCTAssertNotNil(coordinator.navigationController, "It's nil")
    }
    
    func test_01_AppCoordinator_InitialView() {
        coordinator.start(navigationController: navigationController)
        XCTAssertNotNil(coordinator.navigationController?.topViewController as? LoginViewController, "It's not LoginViewController")
    }
    
    func test_02_StateMachine_Default() {
        XCTAssert(coordinator.currentState is UnloggedStateMachine, "It's not UnloggedStateMachine")
    }
    
    func test_02_StateMachine_Login() {
        coordinator.login("test user name")
        XCTAssert(coordinator.currentState is LoggedStateMachine, "It's not LoggedStateMachine")
    }
    
    func test_02_StateMachine_Logout() {
        coordinator.login("test user name")
        coordinator.logout()
        XCTAssert(coordinator.currentState is UnloggedStateMachine, "It's not UnloggedStateMachine")
    }
    
    func test_03_Model_User() {
        let user = DatabaseManager.createNewUser(named: testUserName)
        XCTAssertNotNil(user, "It's nil")
    }
    
    func test_03_Model_User_SetFavoriteArticle() {
        coordinator.login(testUserName)
        let article = ArticleDetail()
        article.id = 123
        article.isFavorite = true
        
        DatabaseManager.updateArticleStatus(article: article)
        let user = DatabaseManager.getCurrentUser()
        
        XCTAssertEqual(user?.favoritesArticles?.count, 1, "It's not one")
    }
    
    func test_03_Model_User_IsFavoriteArticle() {
        coordinator.login(testUserName)
        let article = ArticleDetail()
        article.id = 123
        article.isFavorite = true
        
        DatabaseManager.updateArticleStatus(article: article)
        let user = DatabaseManager.getCurrentUser()
        
        XCTAssertNotNil(user?.isArticleFavorite(articleId: "123"), "It's not favorite")
    }
    
    func test_03_Model_User_ChangeFavoriteArticle() {
        coordinator.login(testUserName)
        let article = ArticleDetail()
        article.id = 123
        article.isFavorite = true
        
        DatabaseManager.updateArticleStatus(article: article)
        let user = DatabaseManager.getCurrentUser()
        
        article.isFavorite = false
        
        DatabaseManager.updateArticleStatus(article: article)
        XCTAssertNil(user?.isArticleFavorite(articleId: "123"), "It's favorite")
    }
    
    func test_03_Model_ArticleList() {
        let articleList = ArticleList()
        XCTAssertNotNil(articleList, "It's nil")
    }
    
    func test_03_Model_ArticleDetail() {
        let articleDetail = ArticleDetail()
        XCTAssertNotNil(articleDetail, "It's nil")
    }
    
    func test_04_RequestAPI_Init() {
        let requestAPI = RequestAPI(networkClient: MockNetwork())
        XCTAssertNotNil(requestAPI, "It's nil")
    }
    
    func test_04_RequestAPI_ListOfNews() {
        let requestAPI = RequestAPI(networkClient: MockNetwork())
        requestAPI.getListOfNews(completionHandler: { (articleList) in
            XCTAssertEqual(articleList.count, 8, "Receive different amount of articles")
        }) { (error) in
            XCTFail(String(describing: error))
        }
    }

    func test_04_RequestAPI_ArticleDetail() {
        let requestAPI = RequestAPI(networkClient: MockNetwork())
        requestAPI.getArticleDetail("100", completionHandler: { (articleDetail) in
            XCTAssertEqual(articleDetail.id, 100, "Receive different article")
        }) { (error) in
            XCTFail(String(describing: error))
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            test_01_AppCoordinator_Init()
            test_02_StateMachine_Default()
            test_02_StateMachine_Login()
            test_02_StateMachine_Logout()
            test_03_Model_User()
            test_03_Model_ArticleList()
            test_03_Model_ArticleDetail()
            test_04_RequestAPI_Init()
            test_04_RequestAPI_ListOfNews()
            test_04_RequestAPI_ArticleDetail()
        }
    }
}
