//
//  MockNetwork.swift
//  FutureWorkshopsApp
//
//  Created by Cristian on 11/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation
@testable import FutureWorkshopsApp

struct MockNetwork: NetworkClient {
    
    func get(baseURL: String, uri: String, params: [String : Any]?, completitionHandler: @escaping FetchDataCompletion, errorHandler: @escaping FetchDataCompletionError) {
        
        let jsonData: Data
        if uri == "fw-coding-test.json" {
            do {
                jsonData = try FileHelper.recoverDataFrom(file: "MockListOfNews", bundle: Bundle(for: FutureWorkshopsAppTests.self), ofType: "json")
                
                if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? jsonObject {
                    completitionHandler(json)
                }
                else {
                    errorHandler(ServerError.unknown)
                }
                
            } catch {
                print(error)
                errorHandler(ServerError.unknown)
            }
        }
        else {
            do {
                jsonData = try FileHelper.recoverDataFrom(file: "MockArticleDetail", bundle: Bundle(for: FutureWorkshopsAppTests.self), ofType: "json")
                
                if let json = try JSONSerialization.jsonObject(with: jsonData, options: []) as? jsonObject {
                    completitionHandler(json)
                }
                else {
                    errorHandler(ServerError.unknown)
                }
            } catch {
                print(error)
                errorHandler(ServerError.unknown)
            }
        }
    }
}

internal func getArticleDetailMock() -> ArticleDetail {
    let article = ArticleDetail()
    article.id = 123
    article.title = "Test title"
    article.image_url = "Test image_url"
    article.source = "Test source"
    article.content = "Test content"
    article.date = Date()
    
    return article
}
