//
//  FileHelper.swift
//  FutureWorkshopsAppTests
//
//  Created by Cristian on 11/03/2018.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import Foundation

public enum FileHelperError: Error {
    case FileNotFound
    case CantReadFile
    case InvalidEncoding
}

internal struct FileHelper {
    
    internal static func readStringFrom(file: String, bundle: Bundle = Bundle.main, ofType: String = "json") throws -> String {
        
        if let fileServices = bundle.path(forResource: file, ofType: ofType) {
            let readedString = try readString(pathFile: fileServices)
            return readedString
        } else {
            throw FileHelperError.FileNotFound
        }
    }
    
    internal static func recoverDataFrom(file: String, bundle: Bundle = Bundle.main, ofType: String = "json") throws -> Data {
        
        if let fileServices = bundle.path(forResource: file, ofType: ofType) {
            let recoveredData = try recoverData(pathFile: fileServices)
            return recoveredData
        } else {
            throw FileHelperError.FileNotFound
        }
    }
    
    private static func recoverData(pathFile: String) throws -> Data {
        do {
            return try Data(contentsOf: URL(fileURLWithPath: pathFile))
        } catch {
            throw FileHelperError.CantReadFile
        }
    }
    
    private static func readString(pathFile: String) throws -> String {
        var jsonData: Data?
        do {
            jsonData = try recoverData(pathFile: pathFile)
        } catch {
            throw error
        }
        
        if let jsonString = String(data: jsonData!, encoding: String.Encoding.utf8) {
            return jsonString
        } else {
            throw FileHelperError.InvalidEncoding
        }
    }
}

