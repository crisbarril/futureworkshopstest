//
//  DetailNewsViewControllerTests.swift
//  FutureWorkshopsAppTests
//
//  Created by Cristian Barril on 12/3/18.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import XCTest
@testable import FutureWorkshopsApp

class DetailNewsViewControllerTests: XCTestCase {
    
    private var viewControllerTest : DetailNewsViewController!
    private var article: ArticleDetail!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        article = getArticleDetailMock()
        
        let articlesListViewModel = DetailNewsViewModel(article)
        viewControllerTest = DetailNewsViewController(viewModel: articlesListViewModel, viewDelegate: nil)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        viewControllerTest = nil
        super.tearDown()
    }
    
    func test_01_ArticleDetail_Init() {
        XCTAssertNotNil(article, "It's nil")
    }
    
    func test_01_ArticleDetail_Properties() {
        XCTAssertNotNil(article, "It's nil")
        XCTAssertNotNil(article.id, "It's nil")
        XCTAssertNotNil(article.title, "It's nil")
        XCTAssertNotNil(article.image_url, "It's nil")
        XCTAssertNotNil(article.source, "It's nil")
        XCTAssertNotNil(article.content, "It's nil")
        XCTAssertNotNil(article.date, "It's nil")
    }
    
    func test_02_ViewController_isViewLoaded() {
        let view = viewControllerTest.view
        XCTAssertNotNil(view, "Can't find view instance")
    }
    
    func test_02_ViewController_ParentViewHasImageViewSubView() {
        let subviews = viewControllerTest.view.subviews
        XCTAssertTrue(subviews.contains(viewControllerTest.articleImage), "ImageView isn't a subview")
    }
    
    func test_02_ViewController_ParentViewHasUILabelTitleSubView() {
        let subviews = viewControllerTest.view.subviews
        XCTAssertTrue(subviews.contains(viewControllerTest.articleTitle), "UILabel isn't a subview")
    }
    
    func test_02_ViewController_ParentViewHasUILabelSubtitleSubView() {
        let subviews = viewControllerTest.view.subviews
        XCTAssertTrue(subviews.contains(viewControllerTest.articleSubtitle), "UILabel isn't a subview")
    }
    
    func test_02_ViewController_ParentViewHasUITextViewSubView() {
        let subviews = viewControllerTest.view.subviews
        XCTAssertTrue(subviews.contains(viewControllerTest.articleContent), "UITextView isn't a subview")
    }

    func test_03_ViewController_ParentViewTitleData() {
        _ = viewControllerTest.view
        let articleTitle = viewControllerTest.articleTitle
        XCTAssertEqual(articleTitle?.text, article.title, "Article title isn't the same")
    }
    
    func test_03_ViewController_ParentViewSubtitleData() {
        _ = viewControllerTest.view
        let articleSubtitle = viewControllerTest.articleSubtitle
        let subtitle = "\(article.source) - \(article.date?.stringFromDate() ?? "no date")"
        XCTAssertEqual(articleSubtitle?.text, subtitle, "Article subtitle isn't the same")
    }

    func test_03_ViewController_ParentViewContentData() {
        _ = viewControllerTest.view
        let articleContent = viewControllerTest.articleContent
        XCTAssertEqual(articleContent?.text, article.content, "Article content isn't the same")
    }
    
    func test_99_PerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
}
