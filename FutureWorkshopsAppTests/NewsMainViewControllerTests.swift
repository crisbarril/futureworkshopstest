//
//  NewsMainViewControllerTests.swift
//  FutureWorkshopsAppTests
//
//  Created by Cristian Barril on 12/3/18.
//  Copyright © 2018 Cristian Barril. All rights reserved.
//

import XCTest
@testable import FutureWorkshopsApp

class NewsMainViewControllerTests: XCTestCase {

    private var viewControllerTest : NewsMainViewController!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        DatabaseManager.bundle = Bundle(for: FutureWorkshopsAppTests.self)
        DatabaseManager.initDatabase()
        let user = DatabaseManager.createNewUser(named: "test user name")
        let articlesListViewModel = NewsMainViewModel(user!)
        viewControllerTest = NewsMainViewController(viewModel: articlesListViewModel, viewDelegate: nil, sessionDelegate: nil)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        try? DatabaseManager.deleteDatabase()
        viewControllerTest = nil
        super.tearDown()
    }
    
    func test_ViewController_isViewLoaded() {
        let view = viewControllerTest.view
        XCTAssertNotNil(view, "Can't find view instance")
    }
    
    func test_ViewController_ParentViewHasTableViewSubView() {
        let subviews = viewControllerTest.view.subviews
        XCTAssertTrue(subviews.contains(viewControllerTest.tableView), "TableView isn't a subview")
    }
    
    func test_ViewController_isTableViewInstance() {
        _ = viewControllerTest.view
        XCTAssertNotNil(viewControllerTest.tableView, "Can't find tableView instance")
    }
    
    func test_ViewController_ThatViewConformsToUITableViewDataSource() {
        XCTAssertTrue(viewControllerTest.conforms(to: UITableViewDataSource.self), "View does not conform to UITableView datasource protocol")
    }
    
    func test_ViewController_ThatTableViewHasDataSource() {
        _ = viewControllerTest.view
        XCTAssertNotNil(viewControllerTest.tableView.dataSource, "Table datasource cannot be nil")
    }
    
    func test_ViewController_ThatViewConformsToUITableViewDelegate() {
        XCTAssertTrue(viewControllerTest.conforms(to: UITableViewDelegate.self), "View does not conform to UITableView delegate protocol")
    }
    
    func test_ViewController_ThatTableViewHasDataDelegate() {
        _ = viewControllerTest.view
        XCTAssertNotNil(viewControllerTest.tableView.delegate, "Table delegate cannot be nil")
    }
    
    func test_ViewController_TableViewNumberOfRowsInSection() {
        let expectedRows = 0
        _ = viewControllerTest.view
        XCTAssertTrue(viewControllerTest.tableView.numberOfRows(inSection: 0) == expectedRows, "Table has \(viewControllerTest.tableView.numberOfRows(inSection: 0)) rows but it should have \(expectedRows)")
    }
    
    func test_ViewController_TableViewNumberOfSections() {
        let expectedSections = 1
        _ = viewControllerTest.view
        XCTAssertTrue(viewControllerTest.tableView.numberOfSections == expectedSections,"Table has \(viewControllerTest.tableView.numberOfSections) sections but it should have \(expectedSections)")
    }
    
    func test_99_PerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            test_ViewController_isViewLoaded()
            test_ViewController_ParentViewHasTableViewSubView()
            test_ViewController_isTableViewInstance()
            test_ViewController_ThatViewConformsToUITableViewDataSource()
            test_ViewController_ThatTableViewHasDataSource()
            test_ViewController_ThatViewConformsToUITableViewDelegate()
            test_ViewController_ThatTableViewHasDataDelegate()
            test_ViewController_TableViewNumberOfRowsInSection()
            test_ViewController_TableViewNumberOfSections()
        }
    }
}
